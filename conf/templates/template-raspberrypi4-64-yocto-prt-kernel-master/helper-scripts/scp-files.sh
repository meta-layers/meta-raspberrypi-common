#!/bin/bash

COMMON_DIR="raspberrypi4-64-yocto-prt-kernel-master/tmp/deploy/images/raspberrypi4-64"

pushd /workdir/build/${COMMON_DIR}

SOURCE_1="core-image-minimal-raspberrypi4-64*.rootfs.wic.*"
SOURCE_2="core-image-minimal-base-raspberrypi4-64*.rootfs.wic.*"

TARGET_1="student@192.168.42.60:/home/student/projects/${COMMON_DIR}"

echo "scp write-sd-card.sh ${TARGET_1}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}
set +x

popd
