#!/bin/bash

COMMON_DIR="raspberrypi3-64-master/tmp/deploy/images/raspberrypi3-64"

pushd /workdir/build/${COMMON_DIR}

SOURCE_1="core-image-base-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${COMMON_DIR}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
