#!/bin/bash

COMMON_DIR="raspberrypi4-64-virt-docker-master/tmp/deploy/images/raspberrypi4-64"

pushd /workdir/build/${COMMON_DIR}

SOURCE_1="core-image-minimal-virt-docker-raspberrypi4-64*.rootfs.wic.*"

TARGET_1="student@192.168.42.60:/home/student/projects/${COMMON_DIR}"

echo "scp write-sd-card.sh ${TARGET_1}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
