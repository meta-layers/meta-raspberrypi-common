#!/bin/bash

COMMON_DIR="raspberrypi4-64-virt-k3s-master/tmp/deploy/images/raspberrypi4-64"

pushd /workdir/build/${COMMON_DIR}

SOURCE_1="core-image-minimal-virt-k3s-host-raspberrypi4-64*.rootfs.wic.*"
SOURCE_2="core-image-minimal-virt-k3s-node-raspberrypi4-64*.rootfs.wic.*"
SOURCE_3="core-image-minimal-virt-k3s-common-raspberrypi4-64*.rootfs.wic.*"

TARGET_1="student@192.168.42.60:/home/student/projects/${COMMON_DIR}"

echo "scp write-sd-card.sh ${TARGET_1}"

set -x
rsync -aP ${SOURCE_1} ${TARGET_1}
rsync -aP ${SOURCE_2} ${TARGET_1}
rsync -aP ${SOURCE_3} ${TARGET_1}
#scp -r ${SOURCE_1} ${TARGET_1}
#scp -r ${SOURCE_2} ${TARGET_1}
set +x

popd
