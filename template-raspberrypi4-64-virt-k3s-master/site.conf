#
# local.conf covers user settings, site.conf covers site specific information
# such as proxy server addresses and optionally any shared download location
#
# SITE_CONF_VERSION is increased each time build/conf/site.conf
# changes incompatibly
SCONF_VERSION = "1"

# Uncomment to cause CVS to use the proxy host specified
#CVS_PROXY_HOST = "proxy.example.com"
#CVS_PROXY_PORT = "81"

# For svn, you need to create ~/.subversion/servers containing:
#[global]
#http-proxy-host = proxy.example.com
#http-proxy-port = 81
#

# To use git with a proxy, you must use an external git proxy command, such as
# the one provided by scripts/oe-git-proxy. To use this script, copy it to
# your PATH and uncomment the following:
#GIT_PROXY_COMMAND ?= "oe-git-proxy"
#ALL_PROXY ?= "socks://socks.example.com:1080"
#or
#ALL_PROXY ?= "https://proxy.example.com:8080"
# If you wish to use certain hosts without the proxy, specify them in NO_PROXY.
# See the script for details on syntax. The script oe-git-proxy uses some tools
# that may not be included on HOSTTOOLS, thus  add them manually through
# HOSTTOOLS += "getent"

# Uncomment this to use a shared download directory
#DL_DIR = "/some/shared/download/directory/"

######################### default settings ###########################

INHERIT += "report-error"
#BB_NUMBER_THREADS = "${@bb.utils.cpu_count()}"
#PARALLEL_MAKE = "-j ${@bb.utils.cpu_count()}"
SDKMACHINE ?= "x86_64"

DL_DIR = "/workdir/downloads_master"
SSTATE_DIR = "/workdir/sstate_master/"

# premirror for u-boot, upstream kernel
# -> gitpod
PREMIRRORS += " \
git://git.denx.de/u-boot.git git://gp@gitpod/u-boot.git;protocol=ssh \n \
git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git git://gp@gitpod/linux-stable.git;protocol=ssh \n \
git://gitlab.denx.de/Xenomai/xenomai.git git://gp@gitpod/xenomai.git;protocol=ssh \n \
"

WARN_QA_append = " version-going-backwards"
ERROR_QA_remove = "version-going-backwards"
#QEMU_USE_KVM = "True"

PRSERV_HOST = "localhost:0"

#BB_GENERATE_MIRROR_TARBALLS = "1"

INHERIT += "own-mirrors"
SOURCE_MIRROR_URL = "http://mirror/source_mirror_master"
#SSTATE_MIRRORS ?= "file://.* http://mirror/sstate_mirror_master/PATH;downloadfilename=PATH"

#PACKAGE_DEBUG_SPLIT_STYLE = "debug-file-directory"

# It is recommended to activate "buildhistory" for testing the PR service
# it also looks like with PR service + buildhistory packages are versioned
# automatically if we change meta data
INHERIT += "buildhistory"
BUILDHISTORY_COMMIT = "1"

# let's only keep the last built image (deprecated)
RM_OLD_IMAGE = "1"

# add buildinfo variables to rootfs /etc/build
INHERIT += "image-buildinfo"
#
# add image-manifest to rootfs /etc/image-manifest
INHERIT += "image-manifestinfo"
#

# --> icecc
# total number of local/builder CPU cores * 2 or * 3 ?
# This variable usually takes the form of “-j x”, 
# where x represents the maximum number of parallel threads make can run.
# needs some playing
# 68 CPU cores total
#ICECC_PARALLEL_MAKE?= "-j ${@oe.utils.cpu_count()*2}"
#ICECC_PARALLEL_MAKE = "-j 24"
#ICECC_PARALLEL_MAKE = "-j 68"
#ICECC_PARALLEL_MAKE = "-j 136"
#ICECC_PARALLEL_MAKE = "-j 204"
# maybe those are not needed?
#PARALLEL_MAKE = "-j 30"
#BB_NUMBER_THREADS = "16"
######################################################################
# Problem:  Icecream and sstate can be combined, however inheriting
#           icecc.bbclass changes most taskhashes
#           Which makes the SSTATE created with icecc unusable for 
#           builds without it.
# Solution: Always inherit icecc.bbclass and use ICECC_DISABLED ?= “1”
#           to turn off icecream for builds without icecc which want to
#           use the SSTATE created with icecc.

#INHERIT += "icecc"
#ICECC_DISABLED ?= "1"
######################################################################

# Blacklist packages from compiling with icecream. There really needs to be a
# better way to share this list...
# let's start with an empty Blacklist and see how things are going
#ICECC_USER_PACKAGE_BL += "glib-2.0 bazel-native tensorflow-native tensorflow pkgconfig json-c linux-yocto-custom linux-raspberrypi"
#                          boost libtevent mpv jack libtalloc libtdb libldb 
# <-- icecc

# --> UNINATIVE override
# UNINATIVE_URL="http://mirror.res.training/releases/uninative/3.0/"
# <-- UNINATIVE override

# --> build minimal stuff
#DISTRO_FEATURES="acl alsa argp bluetooth ext2 ipv4 ipv6 largefile pcmcia usbgadget usbhost wifi xattr nfs zeroconf pci 3g nfc x11 vfat largefile opengl ptest multiarch wayland vulkan pulseaudio sysvinit gobject-introspection-data ldconfig"

#DISTRO_FEATURES_remove = "alsa"
#DISTRO_FEATURES_remove = "bluetooth"
#DISTRO_FEATURES_remove = "pcmcia"
#DISTRO_FEATURES_remove = "wifi"
#DISTRO_FEATURES_remove = "zeroconf"
#DISTRO_FEATURES_remove = "pci"
#DISTRO_FEATURES_remove = "3g"
#DISTRO_FEATURES_remove = "nfc"
#DISTRO_FEATURES_remove = "x11"
#DISTRO_FEATURES_remove = "opengl"
DISTRO_FEATURES_remove = "ptest"
#DISTRO_FEATURES_remove = "wayland"
#DISTRO_FEATURES_remove = "vulkan"
#DISTRO_FEATURES_remove = "pulseaudio"

BB_GIT_SHALLOW = "1"

# <-- build minimal stuff

######################################################################
